var app = require('./config/main')();
var port = process.env.PORT;

app.express.listen(port, function(){
    console.log(`Servidor rodando na porta ${port}!`);
});