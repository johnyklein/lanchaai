const partials = require('express-partials')
const body = require('body-parser');
const expressConfig = require('express');
const cors = require('cors');

module.exports = function() {
    var express = expressConfig();
    express.set('view engine', 'ejs');
    express.use(body.urlencoded({extended:true}));
    express.use(partials());
    express.use(cors());
    express.use(expressConfig.static('./views/'));

    return express;
}