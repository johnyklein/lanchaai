var admin = require("firebase-admin");
var serviceAccount = require("./private-keys/serviceAccountKey.json");

// Add the Firebase products that you want to use
// require("firebase/auth");
require("firebase/database");

var adminConfig = {
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://manager-chatbot.firebaseio.com"
};
   
// Initialize Firebase
module.exports = function() {
    return admin.initializeApp(adminConfig);
}