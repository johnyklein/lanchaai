var firebase = require('firebase');

// Add the Firebase products that you want to use
require("firebase/auth");
require("firebase/database");

var config = {
    apiKey: "AIzaSyDeVnbBMTtx5i4neAVGf3u-qn2W29BLKOI",
    authDomain: "manager-chatbot.firebaseapp.com",
    databaseURL: "https://manager-chatbot.firebaseio.com",
    projectId: "manager-chatbot",
    storageBucket: "manager-chatbot.appspot.com",
    messagingSenderId: "1062442703633",
    appId: "1:1062442703633:web:5630332cb2d1275d"
};
   
// Initialize Firebase
module.exports = function() {
    return firebase.initializeApp(config);
}