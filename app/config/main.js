var load = require('express-load');
var firebaseAdmin = require('./firebase-admin.js')();
var firebase = require('./firebase.js')();
var expressConfig = require('./express.js')();

module.exports = function() {
    var app = {};
    app.express = expressConfig;

    app.firebaseAdmin = firebaseAdmin;
    app.firebase = firebase;

    load('routes', {cwd:'src'}).into(app);

    return app;
}