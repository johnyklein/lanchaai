class Controller {
    constructor(model) {
        this.model = model;
    }
    
    list() {
        return new Promise((resolve, reject) => {
            this.model.list().then((chatbots) => {
                resolve(chatbots);
            });
        })
    }

    get(id) {
        return new Promise((resolve, reject) => {
            this.model.get(id).then((chatbot) => {
            resolve(chatbot);
        });
        })
    }
    
    getByName(name) {
        return new Promise((resolve, reject) => {
            this.model.getByName(name).then((chatbot) => {
            resolve(chatbot);
        });
        })
    }

    add(item) {
        return new Promise((resolve, reject) => {
            this.model.add(item).then(result => {
                resolve(result);
            }).catch(error => {
                reject(error);
            })
        })
    }

    update(item) {
        return new Promise((resolve, reject) => {
            this.model.update(item).then(result => {
                resolve(result);
            }).catch(error => {
                console.log(error);
                reject(error);
            })
        })
    }

    delete(id) {
        return new Promise((resolve, reject) => {
            this.model.delete(id).then((chatbot) => {
                resolve(chatbot);
            });
        })
    }

    deleteAll() {
        return new Promise((resolve, reject) => {
            this.model.deleteAll().then((chatbot) => {
                resolve(chatbot);
            });
        })
    }
}

module.exports = Controller;