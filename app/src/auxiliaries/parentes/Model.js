const msgErrorExists = 'Esse item já existe!';
const msgErrorId = 'Id incorreto!';
const msgSuccessAdd = 'Adicionado com sucesso!';
const msgSuccessUpdt = 'Atualizado com sucesso!';
const msgSuccessDelete = 'Excluido com sucesso!';

const uuid = require('short-uuid');

class Model {
	constructor(firebase, nameCollection) {
		this.firebase = firebase;
		this.nameCollection = nameCollection;
	}

	list() {
		let models = [];
		let collection = this.firebase
							.collection(this.nameCollection);
							
		return new Promise((resolve, reject) => {
			collection.get().then((snapshot) => { 
			  snapshot.forEach((document) => { 
				let model = {};
				model = document.data();
				models.push(model);
			  });
			  resolve(models);
			}).catch((err) => { 
			  reject(err);
			});
		});
	}
	
	get(id) {
		return new Promise((resolve, reject) => {
			this.list().then((resultList) => {
				resultList.forEach(itemList => {
					if (itemList.id === id) {
						resolve(itemList);
					}
				});
				resolve({});
			});
		});
	}

	getByName(name) {
		let models = [];
		let collection = this.firebase
						.collection(this.nameCollection);

		return new Promise((resolve, reject) => {
			collection.get().then((snapshot) => { 
				snapshot.forEach((document) => {
					let actualData = document.data();
					let nameData = actualData.name.toLowerCase();
					if (nameData.includes(name.toLowerCase())) {
						models.push(actualData);
					}
				});
				resolve(models);
			}).catch((err) => { 
				reject(err);
			});
		});
	}
	
	add(item) {
		return new Promise((resolve, reject) => {
			try {
				let collection = this.firebase.collection(this.nameCollection);
				item.id = item.id ? item.id : uuid.generate();
				item.dateMs = Date.now();
				item.dateStr = this.getDateStr(new Date());

				collection = collection.doc(item.id);

				collection.get().then((doc) => {
					if (doc.exists) {
						reject(msgErrorExists);
					} else {
						collection.set(item);
						resolve(msgSuccessAdd);
					}
				})
			} catch (error) {
				reject(error.toString());
			}
		})
	}

	getDateStr(date) {
		return `${(date.getDate())}/${date.getMonth()}/${date.getFullYear()}`;
	}

	update(item) {
		return new Promise((resolve, reject) => {
			try {
				let id = item.id;
				item.dateMs = Date.now();
				let collection = this.firebase.collection(this.nameCollection);
				if (!id) {
					reject(msgErrorId);
				} else {
					collection = collection.doc(id);
					collection.set(item);
					resolve(msgSuccessUpdt);
				}
			} catch (error) {
				reject(error.toString());
			}
		})
	}

	delete(id) {
		return new Promise((resolve, reject) => {
			try {
				let collection = this.firebase.collection(this.nameCollection);
				if (!id) {
					console.log(msgErrorId);
					reject(msgErrorId);
				} else {
					collection = collection.doc(id);
					collection.delete();
					resolve(msgSuccessDelete);
				}
			} catch (error) {
				reject(error.toString());
			}
		});
	}

	deleteAll() {
		return new Promise((resolve, reject) => {
			try {
				this.list().then((resultList) => {
					resultList.forEach(itemList => {
						this.delete(itemList.id)
					});
					resolve({});
				});
			} catch (error) {
				reject(error.toString());
			}
		});
	}

}
module.exports = Model;