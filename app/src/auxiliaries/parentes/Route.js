const errorAuth = 'Usuário não autenticado!'

class Route {
    constructor(controller, auth) {
        this.controller = controller;
        this.auth = auth;
    }

    isAutenticate() {
        // return !this.auth.currentUser === false;
        return true;
    }
       
    list(req, res) {
        if (this.isAutenticate()) {
            this.controller.list()
            .then(result => res.status(200).json(result))
            .catch(error => res.status(400).send(error))
        } else {
            res.status(400).send(errorAuth)
        }
    }

    get(req, res) {
        if (this.isAutenticate()) {
            let id = req.params.id;
            this.controller.get(id)
            .then(result => res.status(200).json(result))
            .catch(error => res.status(400).send(error));
        } else {
            res.status(400).send(errorAuth)
        }
    }


    getByName(req, res) {
        if (this.isAutenticate()) {
            let name = req.params.name;
            this.controller.getByName(name)
            .then(result => res.status(200).json(result))
            .catch(error => res.status(400).send(error));
        } else {
            res.status(400).send(errorAuth)
        }
    }

    add(req, res) {
        if (this.isAutenticate()) {
            let item = req.body;
            this.controller.add(item)
            .then(success => res.status(200).send(success))
            .catch(error => res.status(404).send(error))
        } else {
            res.status(400).send(errorAuth)
        }
    }

    update(req, res) {
        if (this.isAutenticate()) {
            let item = req.body;
            this.controller.update(item)
            .then(success => res.status(200).send(success))
            .catch(error => res.status(404).send(error))
        } else {
            res.status(400).send(errorAuth)
        }
    }

    delete(req, res) {
        if (this.isAutenticate()) {
            let id = req.body.id;
            this.controller.delete(id)
            .then(success => res.status(204).send(success))
            .catch(error => res.status(400).send(error))
        } else {
            res.status(400).send(errorAuth)
        }
    }

    deleteAll(req, res) {
        if (this.isAutenticate()) {
            this.controller.deleteAll()
            .then(success => res.status(204).send(success))
            .catch(error => res.status(400).send(error))
        } else {
            res.status(400).send(errorAuth)
        }
    }
}

module.exports = Route;