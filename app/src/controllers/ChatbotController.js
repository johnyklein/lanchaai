const Controller = require('../auxiliaries/parentes/Controller');

class ChatbotController extends Controller {
  constructor(model) {
    super(model);
  }
}

module.exports = ChatbotController;