const Controller = require('../auxiliaries/parentes/Controller');

class ClientController extends Controller {
  constructor(model) {
    super(model);
  }
}

module.exports = ClientController;