const Controller = require('../auxiliaries/parentes/Controller');

class EstablishmentController extends Controller {
  constructor(model) {
    super(model);
  }
}

module.exports = EstablishmentController;