const Controller = require('../auxiliaries/parentes/Controller');

class OrderController extends Controller {
  constructor(model) {
    super(model);
  }
}

module.exports = OrderController;