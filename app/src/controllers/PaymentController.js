const Controller = require('../auxiliaries/parentes/Controller');

class PaymentController extends Controller {
  constructor(model) {
    super(model);
  }
}

module.exports = PaymentController;