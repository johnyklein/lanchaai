const Controller = require('../auxiliaries/parentes/Controller');

class ProductController extends Controller {
  constructor(model) {
    super(model);
  }
}

module.exports = ProductController;