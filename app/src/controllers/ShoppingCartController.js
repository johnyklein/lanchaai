const Controller = require('../auxiliaries/parentes/Controller');

class ShoppingCartController extends Controller {
  constructor(model) {
    super(model);
  }
}

module.exports = ShoppingCartController;