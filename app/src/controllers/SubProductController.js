const Controller = require('../auxiliaries/parentes/Controller');

class SubProductController extends Controller {
  constructor(model) {
    super(model);
  }
}

module.exports = SubProductController;