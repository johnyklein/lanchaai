const Model = require('../auxiliaries/parentes/Model');

class ChatbotModel extends Model {
	constructor(firebase) {
		super(firebase, 'chatbot');
	}

}
module.exports = ChatbotModel;