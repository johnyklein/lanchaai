const Model = require('../auxiliaries/parentes/Model');

class ClientModel extends Model {
	constructor(firebase) {
		super(firebase, 'client');
	}

}
module.exports = ClientModel;