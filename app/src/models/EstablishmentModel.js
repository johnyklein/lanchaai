const Model = require('../auxiliaries/parentes/Model');

class EstablishmentModel extends Model {
	constructor(firebase) {
		super(firebase, 'establishment');
	}

}
module.exports = EstablishmentModel;