const Model = require('../auxiliaries/parentes/Model');

class OrderModel extends Model {
	constructor(firebase) {
		super(firebase, 'order');
	}

}
module.exports = OrderModel;