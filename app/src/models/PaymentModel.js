const Model = require('../auxiliaries/parentes/Model');

class PaymentModel extends Model {
	constructor(firebase) {
		super(firebase, 'payment');
	}

}
module.exports = PaymentModel;