const Model = require('../auxiliaries/parentes/Model');

class ProductModel extends Model {
	constructor(firebase) {
		super(firebase, 'product');
	}

}
module.exports = ProductModel;