const Model = require('../auxiliaries/parentes/Model');

class ShoppingCartModel extends Model {
	constructor(firebase) {
		super(firebase, 'shoppingCart');
	}

}
module.exports = ShoppingCartModel;