const Model = require('../auxiliaries/parentes/Model');

class SubProductModel extends Model {
	constructor(firebase) {
		super(firebase, 'subproduct');
	}

}
module.exports = SubProductModel;