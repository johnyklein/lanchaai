module.exports = function (app) {
    const express = app.express;
    
    express.get('/', function (req, res) {
        res.render("HomeView", {'urlApi': `https://lanchaai.herokuapp.com/api/`});
    });
}