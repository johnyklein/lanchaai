const bodyParser = require('body-parser').json();
const RoutePath = require('../auxiliaries/parentes/Route');
const ModelPath = require('../models/ChatbotModel');
const ControllerPath = require('../controllers/ChatbotController');

module.exports = function (app) {
    const express = app.express;
    const model = new ModelPath(app.firebase.firestore());
    const controller = new ControllerPath(model);
    const route = new RoutePath(controller, app.firebase.auth());
    
    express.get('/api/chatbot', function (req, res) {
        route.list(req, res);
    });

    express.get('/api/chatbot/:id', function (req, res) {
        route.get(req, res);
    });

    express.post('/api/chatbot/', bodyParser, function (req, res) {
        route.add(req, res);
    });
    
    express.put('/api/chatbot/', bodyParser, function (req, res) {
        route.update(req, res);
    });

    express.delete('/api/chatbot/', bodyParser, function (req, res) {
        route.delete(req, res);
    });

    express.get('/chatbot', function (req, res) {
        res.render('CrudView', getData());
    });
}

function getData() {
    return {
        'pluralName':'Chatbots',
        'singularName': 'Chatbot',
        'propertiesName': ["Código", "Ativo", "Nome"],
        'properties': ["id", "activated", "name"],
        'urlApi': `https://lanchaai.herokuapp.com/api/chatbot/`
    }
}