const bodyParser = require('body-parser').json();
const RoutePath = require('../auxiliaries/parentes/Route');
const ModelPath = require('../models/ClientModel');
const ControllerPath = require('../controllers/ClientController');

module.exports = function (app) {
    const express = app.express;
    const model = new ModelPath(app.firebase.firestore());
    const controller = new ControllerPath(model);
    const route = new RoutePath(controller, app.firebase.auth());
    
    express.get('/api/client', function (req, res) {
        route.list(req, res);
    });

    express.get('/api/client/:id', function (req, res) {
        route.get(req, res);
    });

    express.post('/api/client/', bodyParser, function (req, res) {
        route.add(req, res);
    });

    express.put('/api/client/', bodyParser, function (req, res) {
        route.update(req, res);
    });

    express.delete('/api/client/', bodyParser, function (req, res) {
        route.delete(req, res);
    });
}

function getData() {
    return {
        'pluralName':'Clientes',
        'singularName': 'Cliente',
        'propertiesName': ["Código", "Nome", "Telefone"],
        'properties': ["id", "name", "phoneNumber"],
        'urlApi': `https://lanchaai.herokuapp.com/api/client/`
    }
}