const bodyParser = require('body-parser').json();
const RoutePath = require('../auxiliaries/parentes/Route');
const ModelPath = require('../models/EstablishmentModel');
const ControllerPath = require('../controllers/EstablishmentController');

module.exports = function (app) {
    const express = app.express;
    const model = new ModelPath(app.firebase.firestore());
    const controller = new ControllerPath(model);
    const route = new RoutePath(controller, app.firebase.auth());
    
    express.get('/api/establishment', function (req, res) {
        route.list(req, res);
    });

    express.get('/api/establishment/:id', function (req, res) {
        route.get(req, res);
    });

    express.post('/api/establishment/', bodyParser, function (req, res) {
        route.add(req, res);
    });

    express.put('/api/establishment/', bodyParser, function (req, res) {
        route.update(req, res);
    });

    express.delete('/api/establishment/', bodyParser, function (req, res) {
        route.delete(req, res);
    });

    // express.get('/establishment', function (req, res) {
    //     res.render('CrudView', getData());
    // });
}

// function getData() {
//     return {
//         'pluralName':'Estabelecimentos',
//         'singularName': 'Estabelecimento',
//         'propertiesName': ["Código", "Nome", "CNPJ", "CPF", "Telefone", "Site", "Tipo"],
//         'properties': ["id", "name", "cnpj", "cpf", "phoneNumber", "site", "type"],
//         'urlApi': `https://lanchaai.herokuapp.com/api/establishment/`
//     }
// }