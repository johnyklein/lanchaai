const bodyParser = require('body-parser').json();
const RoutePath = require('../auxiliaries/parentes/Route');
const ModelPath = require('../models/OrderModel');
const ControllerPath = require('../controllers/OrderController');

module.exports = function (app) {
    const express = app.express;
    const model = new ModelPath(app.firebase.firestore());
    const controller = new ControllerPath(model);
    const route = new RoutePath(controller, app.firebase.auth());
    
    express.get('/api/orders', function (req, res) {
        route.list(req, res);
    });

    express.get('/api/orders/:id', function (req, res) {
        route.get(req, res);
    });

    express.post('/api/orders/', bodyParser, function (req, res) {
        route.add(req, res);
    });

    express.put('/api/orders/', bodyParser, function (req, res) {
        route.update(req, res);
    });

    express.delete('/api/orders/', bodyParser, function (req, res) {
        route.delete(req, res);
    });

    express.get('/orders', function (req, res) {
        res.render('CrudView', getData());
    });
}

function getData() {
    return {
        'pluralName':'Pedidos',
        'singularName': 'Pedido',
        'propertiesName': ["Código", "Produtos", "Cliente", "Tipo de Pagamento", "CEP", "Número", "Valor Total", "Data"],
        'properties': ["id", "products", "client", "paymentType", "cep", "houseNumber", "totalValue", "dateStr"],
        'urlApi': `https://lanchaai.herokuapp.com/api/orders/`
    }
}