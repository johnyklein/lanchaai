const bodyParser = require('body-parser').json();
const RoutePath = require('../auxiliaries/parentes/Route');
const ModelPath = require('../models/PaymentModel');
const ControllerPath = require('../controllers/PaymentController');

module.exports = function (app) {
    const express = app.express;
    const model = new ModelPath(app.firebase.firestore());
    const controller = new ControllerPath(model);
    const route = new RoutePath(controller, app.firebase.auth());
    
    express.get('/api/payment', function (req, res) {
        route.list(req, res);
    });

    express.get('/api/payment/:id', function (req, res) {
        route.get(req, res);
    });

    express.post('/api/payment/', bodyParser, function (req, res) {
        route.add(req, res);
    });

    express.put('/api/payment/', bodyParser, function (req, res) {
        route.update(req, res);
    });

    express.delete('/api/payment/', bodyParser, function (req, res) {
        route.delete(req, res);
    });
}