const bodyParser = require('body-parser').json();
const RoutePath = require('../auxiliaries/parentes/Route');
const ModelPath = require('../models/ProductModel');
const ControllerPath = require('../controllers/ProductController');

module.exports = function (app) {
    const express = app.express;
    const model = new ModelPath(app.firebase.firestore());
    const controller = new ControllerPath(model);
    const route = new RoutePath(controller, app.firebase.auth());
    
    express.get('/api/product', function (req, res) {
        route.list(req, res);
    });

    express.get('/api/product/:id', function (req, res) {
        route.get(req, res);
    });

    express.get('/api/productByName/:name', function (req, res) {
        route.getByName(req, res);
    });

    express.post('/api/product/', bodyParser, function (req, res) {
        route.add(req, res);
    });

    express.put('/api/product/', bodyParser, function (req, res) {
        route.update(req, res);
    });

    express.delete('/api/product/', bodyParser, function (req, res) {
        route.delete(req, res);
    });

    express.get('/product', function (req, res) {
        res.render('CrudView', getData());
    });
}

function getData() {
    return {
        'pluralName':'Lanches',
        'singularName': 'Lanche',
        'propertiesName': ["Código", "Nome", "Ativo", "Preço", "Descrição", "Imagem"],
        'properties': ["id", "name", "activated", "price", "description", "image"],
        'urlApi': `https://lanchaai.herokuapp.com/api/product/`
    }
}