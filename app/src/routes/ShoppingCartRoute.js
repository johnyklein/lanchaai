const bodyParser = require('body-parser').json();
const RoutePath = require('../auxiliaries/parentes/Route');
const ModelPath = require('../models/ShoppingCartModel');
const ControllerPath = require('../controllers/ShoppingCartController');

module.exports = function (app) {
    const express = app.express;
    const model = new ModelPath(app.firebase.firestore());
    const controller = new ControllerPath(model);
    const route = new RoutePath(controller, app.firebase.auth());
    
    express.get('/api/shoppingCart', function (req, res) {
        route.list(req, res);
    });

    express.get('/api/shoppingCart/:id', function (req, res) {
        route.get(req, res);
    });

    express.post('/api/shoppingCart/', bodyParser, function (req, res) {
        route.add(req, res);
    });

    express.put('/api/shoppingCart/', bodyParser, function (req, res) {
        route.update(req, res);
    });

    express.delete('/api/shoppingCart/', bodyParser, function (req, res) {
        route.delete(req, res);
    });

    express.delete('/api/shoppingCart-deleteAll/', bodyParser, function (req, res) {
        route.deleteAll(req, res);
    });

    express.get('/shoppingCart/', bodyParser, function (req, res) {
        res.render('ShoppingCartView', getData());
    });
}


function getData() {
    return {
        'pluralName':'Carrinhos',
        'singularName': 'Carrinho',
        'propertiesName': ["Código", "Produto(s)", "Preço(s)", "Quantidade(s)", "Data"],
        'properties': ["id", "products_codes", "prices", "quantities", "dateStr"],
        'urlApi': `https://lanchaai.herokuapp.com/api/shoppingCart/`,
        'urlOrdersApi': `https://lanchaai.herokuapp.com/api/orders/`,
        'urlApiDeleteAll': `https://lanchaai.herokuapp.com/api/shoppingCart-deleteAll`
    }
}