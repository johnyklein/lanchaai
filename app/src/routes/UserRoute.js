const bodyParser = require('body-parser').json();

module.exports = function (app) {
    const express = app.express;
    const firebaseAdmin = app.firebaseAdmin;
    const firebase = app.firebase;
    
    express.get('/api/user/:id', function (req, res) {
        let uid = req.params.id;
        firebaseAdmin.auth().getUser(uid)
        .then(userRecord => res.status(200).json(userRecord))
        .catch(error => res.status(400).json('Error fetching user data:', error));
    });
    
    express.get('/api/userbyemail/:email', function (req, res) {
        let email = req.params.email;
        firebaseAdmin.auth().getUserByEmail(email)
        .then(userRecord => res.status(200).json(userRecord))
        .catch(error => res.status(400).json('Error fetching user data:', error));
    });

    express.get('/api/users/', function (req, res) {
        let nextPageToken = "1";
        firebaseAdmin.auth().listUsers(1000, nextPageToken)
        .then(listUsersResult => res.status(200).json(listUsersResult))
        .catch(error => res.status(400).json(error))
    });
    
    express.post('/api/user/', bodyParser, function (req, res) {
        let user = req.body;
        firebaseAdmin.auth().createUser(user)
        .then(user =>  res.status(200).json(user.email + ' criado com sucesso!'))
        .catch(error => res.status(400).json(error));
    });
    
    express.put('/api/user/', bodyParser, function (req, res) {
        let uid = req.body.uid;
        let userUpdt = req.body.user;
        firebaseAdmin.auth().updateUser(uid, userUpdt)
        .then(user => res.status(400).json('Successfully updated user' + user))
        .catch(error => res.status(400).json('Error updating user: ' + error));
    });
    
    express.delete('/api/user/', bodyParser, function (req, res) {
        let uid = req.body.id;
        firebaseAdmin.auth().deleteUser(uid)
        .then(res.status(200).json('Successfully deleted user'))
        .catch(error => res.status(400).json('Error deleting user:' + error));
    });

    express.post('/api/user/sign', bodyParser, function (req, res) {
        let email = req.body.email;
        let password = req.body.password;
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then(res.status(200).json(email + " autenticado!"))
        .catch(error => res.status(400).json(error))
    });

}