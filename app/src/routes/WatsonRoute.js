const bodyParser = require('body-parser').json();
require('dotenv').config();

module.exports = function (app) {
  const express = app.express;
  var session = new Promise((resolve, reject) => {
    getNewSession().then(sessionData => {
      resolve(sessionData);
    }).catch(err => {
      reject(err);
    })
  });

  express.post('/api/watson/conversation/', bodyParser, (req, res) => {
    let input = req.body.message;
    session.then(sessionData => {
      getResponseMessage(input, session).then(result => {res.json(result)}).catch(err => res.json(err.message));
    }).catch(err => {
      console.log("Timeout de sessão");
      session = getNewSession();
      getResponseMessage(input, session).then(result => {res.json(result)}).catch(err => res.json(err.message));
    })
  });

  express.post('/api/watson/refresh-session/', (req, res) => {
    session = recreateSession(session);
    res.json("ok");
  });
}

const AssistantV2 = require('ibm-watson/assistant/v2');
const service = new AssistantV2({
  iam_apikey: process.env.API_KEY,
  version: '2019-02-28',
  url: 'https://gateway.watsonplatform.net/assistant/api'
});

const _assistant_id =  process.env.ASSISTANT_ID
function getResponseMessage(input_message, session) {
  return new Promise((resolve, reject) => {
    session.then(actualSession => {
      service.message({
        assistant_id: _assistant_id,
        session_id: actualSession.session_id,
        input: {
          'message_type': 'text',
          'text': input_message
          }
        })
        .then(message_resp => {
          resolve(message_resp);
        })
        .catch(err => {
          reject(err);
        });
    })
  }).catch(err => {
    reject(err);
  })
}

function getNewSession() {
  return new Promise((resolve, reject) => {
    service.createSession({assistant_id: _assistant_id})
    .then(result => {
      console.log("Sessão Watson criada com sucesso!");
      resolve(result);
    })
    .catch(err => {
      reject(err);
    })
  });
}

function recreateSession(session) {
  return new Promise((resolve, reject) => {
    session.then(actualSession => {
      service.deleteSession({
        assistant_id: _assistant_id,
        session_id: actualSession.session_id,
      })
      .then(res => {
        console.log("Sessão Watson excluída com sucesso!");
        getNewSession().then(item => {
          resolve(item);
        });
      })
      .catch(err => {
        reject(err);
      });
    }).catch(err => {
      reject(err);
    })
  });
}