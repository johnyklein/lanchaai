function addItem() {
    let idAdd = `#formAdd${pluralName}`;
    
    $(idAdd).submit(function(e){
        e.preventDefault();
        $.ajax({
            url: apiUrlRequest,
            type: 'POST',
            data: $(idAdd).serialize(),
            success: function(msg){
                showSuccess(msg);
                listAll();
                $('.close').click();
                cleanForm();
            },
            error: function(msg) {
                showError(msg.responseText);
            }
        });
    });
}
addItem();

function cleanForm() {
    $(`#formAdd${pluralName}`).each(function(){
       this.reset();
    });
}

function addCodeInAddForm() {
    let code = generatedSmallCode();
    $(`#idAdd${singularName}`).val(code);
}

function eventAddAutoCode() {
    $('.addAutoCode').click(function(){
        addCodeInAddForm();
    });
}
eventAddAutoCode();