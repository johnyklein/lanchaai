function showSuccess(msg) {
    $.jGrowl(msg, {theme: "success-alert shadow", life: 1500});
}

function showError(msg) {
    $.jGrowl(msg, {theme: "error-alert shadow", sticky: true});
}