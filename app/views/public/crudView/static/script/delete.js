function showDeleteItem(event) {
    let code = $(event).attr('data-code');
    addCodeInModal(code);
}

function addCodeInModal(code) {
    $(`#inptDelete${singularName}`).val(code);
}

function deleteItem() {
    let idDelete =`#formDelete${pluralName}`;
    
    $(idDelete).submit(function(e){
        e.preventDefault();
        $.ajax({
            url: apiUrlRequest,
            type: 'DELETE',
            data: $(idDelete).serialize(),
            success: function(){
                showSuccess("Excluído com sucesso!");
                listAll();
                $('.close').click();
            },
            error: function(msg) {
                showError(msg.responseText);
            }
        });
    });
}
deleteItem();