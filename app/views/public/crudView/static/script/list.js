function listAll() {
    addColumnsNRowsInTable();
}
listAll();

function addColumnsNRowsInTable() {
    getDataRequest().then(data => {
        let allHtmlColumns = "";

        if (data.length < 1) $('#btnFishedOrder').addClass("hidden");

        data.forEach(item => {
            if (item && item.id) {
                let htmlColumns = getHtmlTable(item);
                allHtmlColumns += htmlColumns;
            } else {
                console.log("Erro ao listar:");
                console.log(item);
            }
        });

        $(`#tbody-${singularName}`).html(allHtmlColumns);
    });
}

async function getDataRequest() {
    const res = await fetch(apiUrlRequest);
    return await res.json();
}

const getHtmlTable = data => {
    let dinamicRows = getRowsNColumnsTable(data);

    return `<tr> ${dinamicRows} </td>`;
    // return `<tr>
    //     ${dinamicRows}
    //     <td>
    //         <a href="#edit${pluralName}" data-code="${data.id}" onclick="getItem(this)" class="edit" data-toggle="modal">
    //             <i class="material-icons" data-toggle="tooltip" on title="Editar">&#xE254;</i>
    //         </a>
    //         <a href="#delete${pluralName}" data-code="${data.id}" onclick="showDeleteItem(this)" class="delete" data-toggle="modal">
    //             <i class="material-icons" data-toggle="tooltip" title="Deletar">&#xE872;</i>
    //         </a>
    //     </td>
    // </tr>`;
};

function getRowsNColumnsTable(data) {
    let columns = "";
    let minClass = ' class="min"';
    data = getFilteredData(data);

    for (var i=0; i<data.length; i++) {
        let value = data[i];
        minClass = i === 0 ? minClass : '';

        if (value) {
            value = getYesOrNoTd(value);
            value = getImageLabelTd(value);
            // value = getDateTd(value);
        }
        columns += `<td${minClass}> ${value} </td>`;
    }
    
    return columns;
}