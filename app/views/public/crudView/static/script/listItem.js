function getItem(event) {
    let code = $(event).attr('data-code');
    addValuesInForm(code);
}

function addValuesInForm(code) {
    getItemData(code).then(data => {
        setValuesInForm(data);
    });
}

function setValuesInForm(data) {
    data = getFilteredData(data);

    for (var i=0; i<data.length; i++) {
        let value = data[i];
        let elementData = $('.edit-input')[i];

        $(elementData).val(value);  
    }
}

async function getItemData(code) {
    const res = await fetch(apiUrlRequest + code);
    const data = await res.json();
    return data;
}