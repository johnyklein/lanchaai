function getYesOrNoTd(boolean) {
    let value = boolean;
    if (boolean == 'true') return 'Sim';
    else if (boolean == 'false') return 'Não';
    return value;
}

function getDateTd(dateMs) {
    let date = getFormattedDateByMs(dateMs);
    if (date != 'Invalid date') {
        return date;
    }
    return dateMs;
}

function getImageLabelTd(image) {
    if (isExtensionImage(image)) {
        return `<img src="${image}" height="30"></img>`;   
    }
    return image
}

function isExtensionImage(text) {
    if (typeof text === 'string') {
        text = text.toLowerCase();
        isImage = text.includes('png') || text.includes('jpg') || text.includes('svg');
    } else {
        isImage = false;
    }
    
    return isImage;
}

function getComboBoxLabel() {
    return ''; //TODO
}

function getNumberLabel() {
    return ''; //TODO
}

function getPhoneLabel() {
    return ''; //TODO
}

function getCpfCnpjeLabel() {
    return ''; //TODO
}

function getImageLabel() {
    return ''; //TODO
}

function getCashLabel() {
    return ''; //TODO
}

function create_UUID(){
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
}

function generatedSmallCode() {
    return create_UUID().substring(0, 8);
}

function getFilteredData(dataRequest) {
    let values = [];
    for (var i=0; i<propertiesRoute.length; i++) {
        let propertyName = propertiesRoute[i];
        values[i] = dataRequest[propertyName];
    }

    return values;
}

function getFormattedDate(date) {
    return date.format('LLL'); 
}

function getFormattedDateByMs(dateMs) {
    let date = moment.unix(dateMs/1000);
    return getFormattedDate(date);
}