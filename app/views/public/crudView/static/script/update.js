function updateItem() {
    let idUpdate = `#formEdit${pluralName}`;

    $(idUpdate).submit(function(e){
        e.preventDefault();
        $.ajax({
            url: apiUrlRequest,
            type: 'PUT',
            data: $(idUpdate).serialize(),
            success: function(msg){
                showSuccess(msg);
                listAll();
                $('.close').click();
                cleanForm();
            },
            error: function(msg) {
                showError(msg.responseText);
            }
        });
    });
}
updateItem();
