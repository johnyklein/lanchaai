function cepCallback(objContent) {
    let addressInfos = [];
    let watsonCepResponse = getWatsonServiceResponse(objContent.cep);

    for (var prop in objContent) {
        if (objContent[prop]) {
            if (prop !== 'ibge') {
                if (prop === 'bairro') {
                    addressInfos.push('Bairro: ' + objContent[prop]);
                }  else if (prop === 'cep') {
                    addressInfos.push('CEP: ' + objContent[prop]);
                }  else if (prop === 'localidade') {
                    addressInfos.push('Cidade: ' + objContent[prop]);
                }  else if (prop === 'uf') {
                    addressInfos.push('Estado: ' + objContent[prop]);
                } else {
                    addressInfos.push(objContent[prop]);
                }
            }
        }
    }

    watsonCepResponse.then(data => {
        let arrCepResponse = getArrayStringWatson(data);
        let stringArr = addressInfos.concat(arrCepResponse);

        insertWatsonResponsesTemplates(stringArr);
        traceActivitiesByWatsonResponse(data);
    });
}
   
function setInfoCepAndValidate(valor) {
    var isValidate = false;

    //Nova variável "cep" somente com dígitos.
    var cep = valor.replace(/\D/g, '');

    //Verifica se campo cep possui valor informado.
    if (cep != "") {

        //Expressão regular para validar o CEP.
        var validacep = /^[0-9]{8}$/;

        //Valida o formato do CEP.
        if (validacep.test(cep)) {
           //Cria um elemento javascript.
            var script = document.createElement('script');

            //Sincroniza com o callback.
            script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=cepCallback';

            //Insere script no documento e carrega o conteúdo.
            document.body.appendChild(script);

            return !isValidate;
        } //end if.
        else {
            return isValidate;
        }
    } else {
        return isValidate;
    }
}