const textInput = document.getElementById('textInput');
const chat = document.getElementById('chat');
const messageValidResponse = 'Ok, entendi';
const idInptTxt = '#textInput';
const classInptCEP = 'inputToCEP';
const classInptFlavor = 'inputFlavor';

let context = {};

function templateChatMessage(message, from) {
  let content = isSliderProducts(message) ? message : `<p class='messages'>${message}</p>`;
  return `
  <div class="from-${from}">
    <div class="message-inner">
      ${content}
    </div>
  </div>
  `;
} 

// Crate a Element and append to chat
const InsertTemplatesInTheChat = (arrTemplates) => {
  arrTemplates.forEach(item => {
    insertTemplateInTheChat(item);
  });
}

const insertTemplateInTheChat = (template) => {
  const div = document.createElement('div');
  div.innerHTML = template;
  chat.appendChild(div);
  chatScrollDown();
};

// Calling server and get the watson output
const getWatsonMessageAndInsertTemplate = async (text = '') => {
  getWatsonServiceResponse(text).then(data => {
    insertResponseInTheChat(data);
    traceActivitiesByWatsonResponse(data);
  });
};

function traceActivitiesByWatsonResponse(response) {
  var responseToUser = response.output.generic;
  var userAsk = response.output.intents;

  traceActivities(userAsk, responseToUser[0]);
}

const getWatsonServiceResponse = async (text = '') => {
  const uri = `${hostApi}watson/conversation/`;

  return await (await fetch(uri, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      'message': text
    }),
  })).json();
};

textInput.addEventListener('keydown', (event) => {
  if (event.keyCode === 13) {
    sendUserMessage();
  }
});

$(".msg_send_btn").click(function(){
  sendUserMessage();
});

function sendUserMessage() {
  if (textInput.value) {
    let cepValue = getValueByClassName(classInptCEP);
    let flavorValue = getValueByClassName(classInptFlavor);
    
    if (cepValue) {
      responseToCepInput(cepValue);
    } else if (flavorValue) {
      responseToFlavorInput(flavorValue);
    } else {
      sendResposeByWatsonService(textInput.value);
    }

    $('.carousel').carousel('pause');
    
    isertUserInptInChat(textInput.value);
    textInput.value = '';
  }
}

function isertUserInptInChat(valueInput) {
  const template = templateChatMessage(valueInput, 'user');
  insertTemplateInTheChat(template);
}

function responseToCepInput(valueInput) {
  if ( setInfoCepAndValidate(valueInput) ) {
    $(idInptTxt).removeClass(classInptCEP);
    insertWatsonResponsesTemplates(['Ok, seu CEP é válido!', 'Seu endereço é:']);
  } else {
    insertWatsonResponsesTemplates(['CEP inválido!', 'Digite um CEP válido.']);
  }
}

function responseToFlavorInput(valueInput) {
  getAllProductsByName(valueInput).then(function(products) {
    let lengthProd = products.length;
      
    if ( lengthProd  > 0 ) {
      if (lengthProd > 1) {
        insertWatsonResponsesTemplates(['Escolha entre esses sabores:']);
        insertWatsonResponsesTemplates(products.map(a => a.name));
      } else {
        let idx = products[0].idx;
        let id = products[0].id;
        setSelectedProduct(idx);
        setSizeSelected(id);      

        $(idInptTxt).removeClass(classInptFlavor);
        insertWatsonResponseAfterFlavor(valueInput);
        showQuantitySelected();
      }
    } else {
      insertWatsonResponsesTemplates(['Sabor inválido!', 'Escolha um sabor existente.']);
    }

  }); 
}

function setSizeSelected(id) {
  let idSize = `#${id}`;
  if ($(idSize).val() == 0) {
    $(idSize).val("1");
  }  
}

function getValueByClassName(className) {
  if ( $('.' + className).length > 0 ) {
    return $(idInptTxt).val();
  }
  return null;
}

function sendResposeByWatsonService(valueInput) {
    getWatsonMessageAndInsertTemplate(valueInput);
}

function insertResponseInTheChat(response) {
  var responseToUser = response.output.generic;
  reponseChat(responseToUser);
}

function reponseChat(responseToUser) {
  var responses = [];

  responseToUser.forEach(item => {

    if (item.response_type === 'text') { 
      let textReponse = item.text;

      if (textReponse != '{}' && textReponse.trim() != '') {
        if (textReponse.includes('lanche de qual sabor')) { //entity flavor
          $('#textInput').addClass('inputFlavor');
        }
        responses.push(textReponse); //other entities
  
        if (textReponse.includes('tipo de pagamento')) {
          let moneyType = `Dinheiro <i class="fa fa-money" aria-hidden="true"></i>`;
          let cardType = `Cartão de Crédito <i class="fa fa-credit-card" aria-hidden="true"></i>`;
          responses.push(`${moneyType}? Ou ${cardType}?`);
        }
  
        if (textReponse.includes('finalize sua compra')) {
          requestAddShoppingCart();
          responses.push(getShoppingCartMessage());
        }
      }

    } else if (item.title) { //type products
      responses.push(item.title);
      responses.push(getHtmlCarouselProducts());
    }
  });

  insertWatsonResponsesTemplates(responses);

}

function getShoppingCartMessage() {
  let icon = '<i class="fa fa-cart-plus"></i>';
  let link = '<a href="/shoppingCart">carrinho</a>';

  return `Ir ao <label>${link}${icon}</label>`;
}

function requestAddShoppingCart() {
  $.ajax({
    url: `${hostApi}shoppingCart`,
          type: 'POST',
          data: getDataCartFishedOrder(),
          success: function(){
              console.log("Adicionado ao carrinho!")
          }
  });
}

function traceActivities(arrIntents, firstResponse) {
  var intent = arrIntents[0] ? arrIntents[0].intent : '';
  var key = getKeyByAllIntents(intent);
  var validUserValue = key == 0 || isValidResponseUser(firstResponse);
  if (validUserValue) {
    paintActivityCircle(key); 
  }
}

function isValidResponseUser(messageResponse) {
  messageResponse = messageResponse ? messageResponse.text : '';
  return messageResponse.includes(messageValidResponse);
}

function getKeyByAllIntents(arrWatsonIntent) {
  let key;
  getIntentsFixed().forEach(item => {
    let fixedValue = item.name.toLowerCase();
    let watsonValue = arrWatsonIntent.toLowerCase();
    if (fixedValue === watsonValue) {
      key = item.key;
    }
  });
  
  return key;
}

function getIntentsFixed() {
  return [{
    "name": "ZMysaudacao",
    "key": [0]
  },
  {
    "name": "ZMysabor",
    "key": [1]
  },
  {
    "name": "ZMyentrega",
    "key": [2]
  },
  {
    "name": "eCommerce_Method_Of_Payment",
    "key": [3]
  },
  {
    "name": "ZMypedido",
    "key": [4]
  },
  {
    "name": "envio",
    "key": [5]
  }]
}

function insertWatsonResponsesTemplates(arrText) {
  let delayTime = 1000;
  arrText.forEach(item => {
    const template = templateChatMessage(item, 'watson');
    insertResponseWithDelay(template, delayTime);
    delayTime += 800;
  });
}

function insertResponseWithDelay(template, time){
  setTimeout(function(){
    insertTemplateInTheChat(template);

    if (isSliderProducts(template)) {
      insertProductsCarousel('product');
    }
  }, time);

};

function isSliderProducts(htmlStr) {
  return htmlStr.includes('id="contentCarousel"');
}

function chatScrollDown() {
  $(document).ready(function() {
    $('#chat').animate({
        scrollTop: $('#chat').get(0).scrollHeight
    }, 300);
  });
}

function getArrayStringWatson(data) {
  let arrResponse = [];
  data.output.generic.forEach(element => {
      arrResponse.push(element.text);
  });

  return arrResponse;
}

function getAllItensSelected() {
  let qttTypes = 0;
  let qttProducts = 0;
  let products = [];
  
  $(".ipt-product-qtd").each(function() {
    if ($(this).val() >= 1) {
      let id = $(this).attr('id');
      let price = $(this).data('price');
      let qtt = parseInt($(this).val());
      let product = {};
      product.code = id;
      product.qtt = qtt;
      product.price = price;

      qttTypes++;
      qttProducts += qtt;
      products.push(product);
    }
  });

  return {
    'qttTypes': qttTypes,
    'qttProducts': qttProducts,
    'products': products
  }
}

function finishedOrder() {
  let order = {};
  let moreInfo = getOthersOrderInfo();
  order.products = getAllItensSelected().products;
  order.cep = moreInfo.cep;
  order.house_nmbr = moreInfo.house_nmbr;
  order.type_payment = moreInfo.type_payment;

  return order;
}

function getDataCartFishedOrder() {
  let data = {'id': generatedSmallCode()};
  let codes = [];
  let qtts = [];
  let prices = [];
  
  finishedOrder().products.forEach(item => {
    codes.push(item.code);
    qtts.push(item.qtt);
    prices.push(item.price.trim().replace(',', '.'))
  });

  data.prices = prices.toString().replace(',', ', ');
  data.products_codes = codes.toString().replace(',', ', ');;
  data.quantities = qtts.toString().replace(',', ', ');;

  return data;
}