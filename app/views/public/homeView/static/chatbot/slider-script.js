async function getProductsDataRequest(parameter) {
    const res = await fetch(apiUrlRequestProducts + parameter);
    return await res.json();
}
  
function getHtmlCarouselContent(isActive, product) {
  let content = '';
  let active = isActive ? ' active' : '';
  let price = getBrazilianCoin(product.price);

  if (product.image && product.name && product.description) {
    content = `<div class="carousel-item ${active}">
      <div class="card">
      <img class="card-img-top img-fluid" src="${product.image}" alt="Imagem do produto">
      <div class="card-body">
        <h5 class="card-title" data-id="${product.id}" style="font-weight:800">${product.name}</h5>
        <label class="card-text" style="font-weight:normal; font-size:12px; min-height:50px; display:block;">${product.description}</label>
        <label class="card-text" style="font-weight:normal"><small class="text-muted">${price}</small></label>
      </div>
      <label class="card-text col-xs-12" style="font-weight:normal">
      <input class="col-xs-6 product-qtd ipt-product-qtd" data-price="${price}" id="${product.id}" value="0" placeholder="Quantidade" type="text"/>
      <button class="col-xs-3 product-qtd" data-alter="add" data-id="${product.id}" onclick="addRemoveQuantity(this)" type="button">+</button>
      <button class="col-xs-3 product-qtd" data-alter="remove" data-id="${product.id}" onclick="addRemoveQuantity(this)" type="button">-</button>
    </label>
    </div>`;
  }
  
  return content;
}

function addRemoveQuantity(elementClicked) {
  let id = $(elementClicked).data('id');
  let $input = $(`#${id}`);
  let iputVal = parseInt($input.val());
  let typeAlter = $(elementClicked).data('alter');

  if (typeAlter === 'add') {
    $input.val(iputVal+1);
  } else if (typeAlter === 'remove') {
    if (iputVal-1 >= 0) {
      $input.val(iputVal-1);
    }
  }
  showQuantitySelected();
}

function showQuantitySelected() {
  let  productsSelected = getAllItensSelected();
  let qttTypes = productsSelected.qttTypes;
  let qttProducts = productsSelected.qttProducts;
  $('#totalTypes').html(qttTypes);
  $('#totalItens').html(qttProducts);
}

function getBrazilianCoin(value) {
  var atual = value ? parseFloat(value) : -1;
  return atual.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'});
}

function insertProductsCarousel(parameter) {
  $('#contentCarousel').append("<label id='wait-product'>Aguarde...</label>");

  getProductsDataRequest(parameter).then(data => {
    let htmlCarousel = "";
    let isActive = true;
    data.forEach(item => {
      htmlCarousel = getHtmlCarouselContent(isActive, item);
      $('#wait-product').remove();
      $('#contentCarousel').append(htmlCarousel);
      isActive = false;
    });
    $('#contentCarousel').append(getEventProductClick());
    $('#contentCarousel').append(getEventsCarousel());

  }).catch(err => {
    console.log(err);
  });
}

function getEventProductClick() {
  return `<script>
    $('.card').click(function(){
      clickedProduct($(this));
    });
  </script>`;
}

function getEventsCarousel() {
  return `<script>
    (function ($) {
      // Control buttons
      $('.next').click(function () {
        $('.carousel').carousel('next');
        return false;
      });
      $('.prev').click(function () {
        $('.carousel').carousel('prev');
        return false;
      });
    
      // On carousel scroll
      $("#productsCarousel").on("slide.bs.carousel", function (e) {
        var $e = $(e.relatedTarget);
        var idx = $e.index();
        var itemsPerSlide = 3;
        var totalItems = $(".carousel-item").length;
        if (idx >= totalItems - (itemsPerSlide - 1)) {
          var it = itemsPerSlide - (totalItems - idx);
          for (var i = 0; i < it; i++) {
            // append slides to end 
            if (e.direction == "left") {
              $(".carousel-inner").append($(".carousel-item").eq(i));
            } else {
              $(".carousel-item").eq(0).appendTo(".carousel-inner");
            }
          }
        }
      });
    })
    (jQuery);
  </script>
  `;
}

async function getAllProductsByName(name) {
  let products = getAllProductsCarouselByName(name);
  let productsRequest = await getProductsDataRequest(`productByName/${name}`);
  if (products.length <= 0) {
    products = productsRequest;
  }
  return products;
}

function getAllProductsCarouselByName(name) {
  var products = [];
  $(".card-title").each(function () {
    let text = $(this).text().toLowerCase();
    let index = $(".card-title").index(this);
    let elementTitle = $(".card-title").eq(index);
    let id = $(elementTitle).data('id');
    if (name.toLowerCase().includes(text)) {
      products.push({'name': text, 'idx': index, 'id': id});  
    }
  });

  return products;
}

function getHtmlCarouselProducts() {
  return `<div class="col-xs-12 text-right">
          <button class='btn btn-default btn-xs'>Total de itens: <span class="qttProduct" id="totalItens">0</span></button>
          <button class='btn btn-default btn-xs'>Total de sabores: <span class="qttProduct" id="totalTypes">0</span></button>
        </div>
        <div class="container-fluid show-products">
          <div id="productsCarousel" class="carousel slide" data-ride="carousel">
            <div id="contentCarousel" class="carousel-inner row w-100 mx-auto ">
            </div>
          </div>
          <div class="row">
            <div class="col-12 text-center mt-4">
              <a class="btn btn-outline-secondary mx-1 prev" href="javascript:void(0)" title="Anterior">
                <i class="fa fa-lg fa-chevron-left"></i>
              </a>
              <a class="btn btn-outline-secondary mx-1 next" href="javascript:void(0)" title="Próximo">
                <i class="fa fa-lg fa-chevron-right"></i>
              </a>
            </div>
          </div>
        </div>`;
}

function getProductMessage(divProduct) {
  let productChoosed = $(divProduct).find('h5').html();
  let msg = `Desejo escolher o sabor ${productChoosed}`;
  $('#textInput').val(msg);
  insertMessageMoreOneProducts(msg);
}

function insertMessageMoreOneProducts(message) {
  let qtt = $('#totalTypes').html();
  if (parseInt(qtt) > 1) {
    $('#textInput').val(`${message} e outro(s)...`);
  }
}

function setSelectedProduct(idx) {
  let elementCarousel = $('.carousel-item').eq(idx);
  let elementCard = $('.card').eq(idx);
  setActiveProduct(elementCarousel);
  setBorderProduct(elementCard);
};

function clickedProduct(product) {
  getProductMessage(product);
  setBorderProduct(product);
}

function setBorderProduct(element) {
  $('.card').css("border", "1px solid rgba(0, 0, 0, 0.125)");
  $(element).css("border", "1px solid rgb(0, 0, 0)");
}

function setActiveProduct(elementCarousel) {
  $('.carousel-item').removeClass('active');
  $(elementCarousel).addClass('active');
}

function insertWatsonResponseAfterFlavor(messageFlavor) {
  let watsonFlavorResponse = getWatsonServiceResponse(messageFlavor);
  watsonFlavorResponse.then(data => {
    let messagesResponse = getArrayStringWatson(data);
    insertWatsonResponsesTemplates(messagesResponse);
    traceActivitiesByWatsonResponse(data);
    $('#textInput').addClass('inputToCEP');
});
}

function getOthersOrderInfo() {
  let info = {};
  $('.messages').each(function(index) {
    let messageLast = $('.messages').eq(index-1);
    
    if ($(this).html() === 'Ok, seu CEP é válido!') {
      info.cep = messageLast.html();
    } else if ($(this).html() === 'Ok, entendi qual o número da casa.') {
      info.house_nmbr = messageLast.html();
    } else if ($(this).html() === 'Ok, entendi a forma de pagamento.') {
      info.type_payment = messageLast.html();
    }
  });

  return info;
}