function paintActivityCircle(index) {
    let element1 = $('.rad-list-group-item').eq(index);
    let element2 = $('.rad-list-icon').eq(index);
    let element3 = $('.md-text').eq(index);
    completeActivityStep(element1, element2, element3);
}

function completeActivityStep(element1, element2, element3) {
    $(element1).css('opacity', '1');
    $(element2).html('<i class="fa fa-check"></i>');
    $(element2).css('background-color', '#1e7936');
    $(element3).css('padding', '0px');
    $(element3).html(getFormattedDate(moment()));
}